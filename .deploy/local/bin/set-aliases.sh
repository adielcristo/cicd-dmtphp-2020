#!/usr/bin/env bash

alias composer="docker-compose \
    exec php composer $@"

alias console="docker-compose \
    exec php bin/console $@"

alias docker-compose="docker-compose \
    --project-directory ${COMPOSE_PROJECT_DIR} \
    --file ${COMPOSE_PROJECT_DIR}/docker-compose.yaml \
    $@"

alias phpunit="docker-compose \
    exec php vendor/bin/simple-phpunit $@"

alias symfony="docker-compose \
    exec php symfony $@"
